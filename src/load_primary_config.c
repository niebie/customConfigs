/*
  TODO > File information.
 */

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#ifndef MAIN_CONFIG
#define MAIN_CONFIG "default.lua"
#endif

#define _handleLuaResult(res, failLbl)                                  \
  switch( res )                                                         \
    {                                                                   \
    case LUA_OK:                                                        \
      /* We parsed the main configuration file successfully! */         \
      break;                                                            \
    case LUA_ERRSYNTAX:                                                 \
      /* The pre-compilation of the configuration file failed! */       \
      printf( "Syntax error!\n" );                                      \
      goto failLbl;                                                     \
    case LUA_ERRMEM:                                                    \
      /* Uh we can't run out of memory, shit. */                        \
      printf( "Memory allocation failure!\n" );                         \
      goto failLbl;                                                     \
    case LUA_ERRGCMM:                                                   \
      /* Garbage collectors are shite anyways. */                       \
      printf( "GC failure\n" );                                         \
      goto failLbl;                                                     \
    case LUA_ERRFILE:                                                   \
      /* File does not exist! */                                        \
      printf( "File does not exist!\n" );                               \
      goto failLbl;                                                     \
    default:                                                            \
      /* This is a fatal error, the LUA library is confused. */         \
      printf( "LUA CALL FAILED (%d)! JUMPING TO FAILURE LABEL\n", res ); \
      goto failLbl;                                                     \
    }

static lua_State *get_lua_state( )
{
  static lua_State *sLuaState = NULL;

  if( sLuaState == NULL )
    {
      sLuaState = luaL_newstate( );
      luaL_openlibs( sLuaState );
    }
  else
    {
      // NOTE > Used for debugging statements.
    }
  return sLuaState;
}

int load_primary_config( const char *aFileName )
{
  // We only want to have ONE state object for the primary config.
  lua_State *lLuaState = get_lua_state( );

  if( lLuaState != NULL )
    {
      _handleLuaResult( luaL_loadfile( lLuaState, aFileName ),
                        _L_FILE_LOAD_FAILURE );
      return 0;

    _L_FILE_LOAD_FAILURE:
      printf( "File '%s' failed to load!\n", aFileName );
      goto _L_FAILURE;
    }
  else
    {
    _L_FAILURE:
      // TODO > set a library error code...?
      return -1;
    }
}
