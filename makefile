linkLibs=-llua
createLib=-Wall -fPIC -shared
files=./src/load_primary_config.c
output=-o ./lib/liblpc.so

all:
	mkdir -p ./bin
	mkdir -p ./lib
	rm -rf ./bin/*
	rm -rf ./lib/*
	gcc $(createLib) $(files) $(output) $(linkLibs)
